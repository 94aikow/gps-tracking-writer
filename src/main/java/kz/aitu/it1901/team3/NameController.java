package kz.aitu.it1901.team3;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NameController
{
    final
    NamesRepository namesRepository;

    public NameController(NamesRepository namesRepository) {
        this.namesRepository = namesRepository;
    }

    @GetMapping({ "/add-tracking" })
    public String hello(@RequestParam(value = "loc") final String location) {
        try {
            this.namesRepository.insertName(location);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return String.format("Your coordinates is %s", location);
    }

}
