FROM openjdk:12
ADD ./target/writer.jar writer.jar
ENTRYPOINT [ "java", "-Xmx64m", "-Xms64m", "-jar", "/writer.jar" ]
